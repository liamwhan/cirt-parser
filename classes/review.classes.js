"use strict";


module.exports = {
    Unit: Unit,
    Person: Person,
    Section: Section,
    Review: Review,
    Response: Response,
    ActionItem: ActionItem,
    ReviewItem: ReviewItem,
    Comment: Comment,
    Indicator: Indicator,
    IndicatorData: IndicatorData,
    SectionData: SectionData,
    PersonData: PersonData,
    ReviewData: ReviewData
};

/**
 * @namespace Unit
 * @prop id {Number}
 * @prop orgUnit {String}
 * @prop costCentre {String}
 * @prop district {String}
 * @prop type {String}
 * @param opts
 * @constructor
 */
function Unit(opts) {
    this.id = opts.id || null;
    this.orgUnit = opts.orgUnit || null;
    this.costCentre = opts.costCentre || null;
    this.district = opts.district || null;
    this.type = opts.type || null;
    
    if(opts.cluster) {
        this.cluster = opts.cluster;
    }
    
    if(opts.unitClass) {
        this.unitClass = opts.unitClass;
    }
    
    if(opts.serviceType) {
        this.serviceType = opts.serviceType;
    }
    
    
}

/**
 * @namespace Person
 * @class
 * @param opts {Object}
 * @param review {Review} The review object that this Person is a member of
 * @constructor
 * @prop {Review} $$review The parent review this Person is a member of
 * @prop {String} cisId CIS identifier from CIS output
 * @prop {String} costCentre Unit CostCentre
 * @prop {String} district District
 * @prop {String} firstName The Person's first name
 * @prop {String} lastName The Person's last name
 * @prop {Number} id The person's global identifier from the Residents data, helps lookups.
 * @prop {String} serviceType Only present for People who live in an SSL or LRC, as there are defining types of these units
 * @prop {String} type An abbreviated version of the serviceType used to identify the correct Indicator Types
 * @prop {Number} unitId The global identifier for the unit in the Units data, helps lookups.
 *
 */
function Person(opts, review) {

    this.cisId = opts.cisId || null;
    this.cluster = opts.cluster || null;
    this.costCentre = opts.costCentre || null;
    this.district = opts.district || null;
    this.dob = opts.dob || null;
    this.firstName = opts.firstName || null;
    this.id = opts.id || null;
    this.lastName = opts.lastName || null;
    this.orgUnit = opts.orgUnit || null;
    this.serviceType = opts.serviceType || null;
    this.type = opts.type || null;
    this.unitId = opts.unitId || null;

    this.$$review = review || null;

    this.sections = opts.sections || [];

    this.initSections = () => {

        if (this.sections.length > 0) {

            this.sections = this.sections.map((section) => {


                var _section = section;
                var indicators = section.indicators;
                if (!(section instanceof Section)) {
                    _section = new Section(section, this.$$review);
                }
                _section.loadIndicators(indicators);


                return _section;
            });


        } else {

            let sections = sectionsService(this.type).filter((section) => section.category == "PERSON");

            let indicators = (/LRC|SSL/.test(this.$$review.unitType))
                ? indicatorsService(this.$$review.unitType, this.$$review.unit.unitClass)
                : indicatorsService(this.$$review.unitType);

            // $log.debug("Person::initSections unitType", this.$$review.unitType);
            // $log.debug("Person::initSections unitClass", this.$$review.unit.unitClass);
            // $log.debug("Person::initSections indicators", indicators);


            this.sections = sections.map((section) => {

                section = new Section(section, this.$$review);
                section.loadIndicators(indicators);

                return section;

            });


        }



    };

    /**
     * @name Person#getSection
     * @param $stateParams
     * @returns {Section}
     */
    this.getSection = function ($stateParams) {
        return this.sections.find(function (section) {
            return section.category == $stateParams.category && section.id == $stateParams.section;
        })
    };

    /**
     * @name Person#nextSection
     * @param $stateParams
     * @returns {Section}
     */
    this.nextSection = ($stateParams) => {
        var currentSection = this.getSection($stateParams);
        var nextIdx = this.sections.indexOf(currentSection) + 1;
        return this.sections[nextIdx];
    };

    /**
     * Returns the Section before the current section regardless of category
     * @name Person#prevSection
     * @param $stateParams
     * @returns {Section}
     */
    this.prevSection = ($stateParams) => {
        var currentSection = this.getSection($stateParams);
        var prevIdx = this.sections.indexOf(currentSection) - 1;
        return this.sections[prevIdx];
    };


    /**
     * @name Person#saveReview()
     * Saves Review to disk
     */
    this.saveReview = () => {
        ReviewResource.saveReview(this.$$review);
    };


    /**
     * Returns true if the person contains no action items false otherwise
     * @name Person#isEmpty
     * @returns {Boolean}
     */
    this.isEmpty = () => {
        var isEmpty = false;
        isEmpty = this.sections.every((section) => {
            return section.isEmpty();
        });

        return isEmpty;
    };

}

/**
 * @namespace Section
 * @class
 * @param section
 * @param {Review} review
 * @prop {Number} id
 * @prop {String} name The section's name. Printed to Section view headers
 * @prop {'PERSON'|'HOME'} category Denotes whether the section contains HOME or PERSON indicators
 * @prop {Review} $$review The review object that the section is a member of
 * @prop {Array.<Indicator>} indicators Array of indicators in this section
 *
 * @constructor
 */
function Section(section, review) {

    this.id = section.id;
    this.name = section.name;
    this.category = section.category;

    this.$$review = review || null;

    this.indicators = section.indicators || [];

    /**
     * @name Section#loadIndicators
     * @param indicators
     * @returns {Section}
     */
    this.loadIndicators = (indicators) => {

        let sectionIndicators = indicators.filter((item) => item.sectionId == this.id && item.category == this.category);

        this.indicators = sectionIndicators.map((item) => {

            return new Indicator(item, this);

        });


        return this;

    };

    /**
     * @name Section#initIndicators
     * Initialises existing indicators
     */
    this.initIndicators = () => {

        if (this.indicators.length > 0) {
            this.indicators = this.indicators.map((indicator) => {
                if (!(indicator instanceof Indicator)) {
                    indicator = new Indicator(indicator, this);
                    return indicator;
                }
            });
        }
    };

    /**
     * @name Section#getIndicator
     * @param id {Number}
     * @returns {Indicator}
     */
    this.getIndicator = (id) => {

        return this.indicators.find(function (item) {

            return item.id == id;

        });

    };


    /**
     * Returns all indicators where indicator.parent == 0
     * @name Section#getParentIndicators
     * @returns {Array.<Indicator>}
     */
    this.getParentIndicators = function () {

        return this.indicators.filter((x) => x.parent === 0);

    };

    /**
     * Returns an array of indicators that have a parent
     * @name Section#getChildIndicators
     * @param {Indicator} indicator
     * @returns {Array.<Indicator>}
     */
    this.getChildIndicators = function (indicator) {

        return this.indicators.filter((x) => x.parent !== 0);

    };

    /**
     * Returns an array of child indicators for a given parent indicator
     * @param indicator
     * @returns {Array.<Indicator>}
     */
    this.getChildrenOf = function (indicator) {

        return this.indicators.filter((x) => x.parent === indicator.id);

    };

    /**
     * Returns true if the section contains no action items false otherwise
     * @name Section#isEmpty
     * @returns {Boolean}
     */
    this.isEmpty = () => {
        var isEmpty = false;
        isEmpty = this.indicators.every((indicator) => {
            return indicator.actionItems.length === 0;
        });

        return isEmpty;
    };

    this.initIndicators();


}

/**
 * The root node in a CIRT review tree, contains all of the sections filtered by unit type
 * @namespace Review
 * @class
 * @param opts {Object} POJO containing primitive data for the Review
 * @prop {String} Universal Unique Id for deduping
 * @prop {Number} unitId
 * @prop {String} unitType
 * @prop {Object} unit An object representing the ADHC unit this review is conducted on.
 * @prop {String} createdBy The username of the user who created the review
 * @prop {Date} dateCreated The date the review was created
 * @prop {Date} lastEdit the datetime the review was last editted
 * @prop {String} lastEditBy The username of the user who last editted the review
 * @prop {Array.<Section>} sections an array of Sections in this review
 * @prop {Array.<Person>} people An array of the people in the unit
 * @constructor
 *
 */
function Review(opts) {

    this.id = opts.id || uuid.generate();
    this.unitId = opts.unitId || null;
    this.unitType = opts.unitType || null;
    this.unit = opts.unit || null;
    this.createdBy = opts.createdBy || null;
    this.dateCreated = opts.dateCreated || null;
    this.lastEdit = new Date();
    this.lastEditBy = opts.lastEditBy || null;

    this.filename = opts.filename || './data/reviews/review-' + this.unitId + '.json';

    this.sections = opts.sections || [];
    this.people = opts.people || [];

    this.loadSections = function (sections, indicators) {

        var _sectionsUnit = sections.filter((sec) => sec.category == "HOME");
        this.sections = _sectionsUnit.map((section) => {

            if (!(section instanceof Section)) {
                var _section = new Section(section, this);

            }

            _section.loadIndicators(indicators);
            return _section;
        });

        return this;
    };

    /**
     * @name Review#initSections
     * Initialises exsiting section data
     */
    this.initSections = () => {

        if (this.sections.length > 0) {
            // init existing sections
            this.sections = this.sections.map((section) => {
                var indicators = section.indicators;
                var _section = new Section(section, this);
                _section.loadIndicators(indicators);

                return _section;
            })
        } else {

            let LRCSSLpattern = /LRC|SSL/;
            let _sections = sectionsService(this.unitType);
            let sections = _sections.filter((section) => section.category == "HOME");



            let indicators = (LRCSSLpattern.test(this.unitType))
                ? indicatorsService(this.unitType, this.unit.unitClass)
                : indicatorsService(this.unitType);


            this.loadSections(sections, indicators);


            this.people = this.unit.people;

        }
    };

    /**
     * @name Review#getSection
     * @param $stateParams
     * @returns {Section}
     */
    this.getSection = function ($stateParams) {
        return this.sections.find(function (section) {
            return section.category == $stateParams.category && section.id == $stateParams.section;
        })
    };

    /**
     * @name Review#nextSection
     * @param $stateParams
     * @returns {Section}
     */
    this.nextSection = ($stateParams) => {
        var currentSection = this.getSection($stateParams);
        var nextIdx = this.sections.indexOf(currentSection) + 1;
        return this.sections[nextIdx];
    };

    /**
     * Returns the Section before the current section regardless of category
     * @name Review#prevSection
     * @param $stateParams
     * @returns {Section}
     */
    this.prevSection = ($stateParams) => {
        var currentSection = this.getSection($stateParams);
        var prevIdx = this.sections.indexOf(currentSection) - 1;
        return this.sections[prevIdx];
    };


    /**
     * @name Review#saveReview()
     * Saves Review to disk
     */
    this.saveReview = () => {
        ReviewResource.saveReview(this);
    };

    /**
     * Returns an array of sections filtered by a given category
     * @name Review#getSectionsByCategory
     * @param category {String}
     * @returns {Array.<Section>}
     */
    this.getSectionsByCategory = (category) => {

        return this.sections;

    };

    /**
     * Returns true if the review contains no saved data false otherwise
     * @name Review#isEmpty
     * @returns {Boolean}
     */
    this.isEmpty = () => {
        var isEmpty = false;
        this.sections.forEach((section) => {
            isEmpty = section.indicators.every((indicator) => {
                return indicator.actionItems.length === 0 && indicator.comments == null && indicator.response == null;
            })
        });

        return isEmpty;
    };

    /**
     * Returns true if the review contains no action items false otherwise
     * @name Review#isActionsEmpty
     * @returns {Boolean}
     */
    this.isActionsEmpty = () => {
        var isEmpty = true;
        this.sections.every((section) => {
            isEmpty = section.indicators.every((indicator) => {
                return indicator.actionItems.length === 0;
            });
            return isEmpty;
        });

        return isEmpty;
    };

    /**
     * Returns true if there's no action items for people
     * @name Review#isPeopleActionsEmpty
     * @returns {Boolean}
     */
    this.isPeopleActionsEmpty = () => {
        var isEmpty = false;
        this.people.every((person) => {
            isEmpty = person.sections.every((section) => {
                return section.indicators.every((indicator) => {
                    return indicator.actionItems.length === 0;
                });
            });
        });

        return isEmpty;
    };

    /**
     * Adds section indicators per person
     * @name Review#initPeople
     *
     */
    this.initPeople = function () {


        if (this.people.length > 0) {

            this.people = this.people.map((person) => {


                if (!(person instanceof Person)) {
                    person = new Person(person, this);
                }

                person.initSections();

                return person;
            });


        } else {


            this.people = this.unit.people;
            this.initPeople();
        }
    };

    /**
     * Initialises sections and people
     */
    this.init = function () {
        this.initSections();
        this.initPeople();
    };

    this.init();

}

/**
 * Response class, was used to hold REsponse data, unneeded as Responses are now handled as  primitves.
 * @deprecated
 * @param indicator
 * @param value
 * @constructor
 */
function Response(indicator, value) {

    if (angular.isUndefined(indicator) || !(indicator instanceof Indicator)) {
        throw new Error("An indicator must be passed to the constructor")
    }

    var _dataType = "RESPONSE";
    this.$$indicator = indicator;
    this.value = value || null;
    this.hasAnswer = function () {
        return this.value !== null
    };

    this.isEmpty = !this.hasAnswer();

    this.getType = function () {
        return _dataType;
    }

}

/**
 * Action Item Class - hold AI data
 * @param indicator {Indicator}
 * @param data {Object}
 * @constructor
 */
function ActionItem(indicator, data) {


    if (angular.isUndefined(indicator) || !(indicator instanceof Indicator)) {
        throw new Error("An indicator must be passed to the constructor");
    }

    this.$$indicator = indicator;
    this.id = (data && data.id) ? data.id : null;
    this.dueDate = (data && data.dueDate) ? new Date(data.dueDate) : null;
    this.staffAssigned = (data && data.staffAssigned) ? data.staffAssigned : null;
    this.details = (data && data.details) ? data.details : null;
    this.status = (data && data.status) ? data.status : 'Not Started';
    this.reviewItems = (data && data.reviewItems) ? data.reviewItems : [];

    var _dataType = "ACTION_ITEM";

    this.getType = function () {

        return _dataType;

    };

    this.isEmpty = function () {
        return this.dueDate === null
            && this.staffAssigned === null
            && this.details === null;

    };

    this.newReviewItem = function () {
        return new ReviewItem(this);
    };

    this.saveReviewItem = function (reviewItem) {
        if (!(reviewItem instanceof ReviewItem)) {
            $log.error("ActionItem::saveReviewItem() - parameter reviewItem must be instance of ReviewItem class. " +
                "Please use ActionItem::newReviewItem() to instantiate new ReviewItems", reviewItem);
        }

        if (reviewItem.id !== null && this.getReviewItem(reviewItem.id) !== undefined) {
            //objects are updated by 2 way data binding. so just return
            return;
        }

        reviewItem.id = uuid.generate();

        this.reviewItems.push(reviewItem);
    };

    this.removeReviewItem = function (reviewItem) {
        var idx = this.reviewItems.indexOf(reviewItem);
        this.reviewItems.splice(idx, 1);
        return this;
    };


    this.getReviewItem = function (id) {
        return this.reviewItems.find((ai) => ai.id == id);
    };

    this.reviewItems = this.reviewItems.map((reviewItem) => {
        if (!(reviewItem instanceof ReviewItem)) {
            reviewItem = new ReviewItem(this, reviewItem);
        }

        return reviewItem;
    });

}

/**
 * Review Item Class - hold RI data
 * @param actionItem
 * @param data
 * @constructor
 */
function ReviewItem(actionItem, data) {


    if (angular.isUndefined(actionItem) || !(actionItem instanceof ActionItem)) {
        throw new Error("An actionItem must be passed to the constructor");
    }

    this.$$actionItem = actionItem;
    this.id = (data && data.id) ? data.id : null;
    this.updateDate = (data && data.updateDate) ? new Date(data.updateDate) : new Date();
    this.updatedBy = (data && data.updatedBy) ? data.updatedBy : null;
    this.status = (data && data.status) ? data.status : 'Not Started';
    this.comment = (data && data.comment) ? data.comment : null;

    var _dataType = "REVIEW_ITEM";

    this.getType = function () {

        return _dataType;

    };

    this.isEmpty = function () {
        return this.comment === null;
    };

}

/**
 * Comment Class - holds comment data
 * @param indicator
 * @param data
 * @constructor
 */
function Comment(indicator, data) {


    if (angular.isUndefined(indicator) || !(data.indicator instanceof Indicator)) {
        throw new Error("An indicator must be passed to the constructor")
    }


    this.$$indicator = indicator;
    this.value = data || null;
    this.hasAnswer = (this.value !== null);
    this.isEmpty = !this.hasAnswer;

    var _dataType = "COMMENT";

    this.getType = function () {
        return _dataType;
    }

}

/**
 * Indicator Class - holds indicator text and response data
 * @param options
 * @param section
 * @prop $$section {Section}
 * @prop $$parent {Indicator}
 * @prop id {String|Number}
 * @prop parent {Number}
 * @prop text {String}
 * @prop instruction {String}
 * @prop sectionId {Number}
 * @prop responseType {'DATE'|'YESNO'|'QUANTITY'}
 * @prop action {String}
 * @prop category {'HOME'|'PERSON'}
 * @prop type {String} Unit Type
 * @prop ranking {'HIGH'|'MEDIUM'|'LOW'}
 * @prop workstream {'ADMIN'|'OPERATIONS'}
 * @prop reviewType {String}
 * @prop showSmall {Boolean}
 * @prop showLarge {Boolean}
 * @prop hasChildren {Boolean}
 * @prop children {Array.<Number>}
 * @prop actionItems {Array.<ActionItem>}
 * @prop comments {String}
 * @prop evidence {String}
 * @prop response {Response}
 * @constructor
 */
function Indicator(options, section) {

    this.id = options.id;
    this.code = options.code;
    this.parent = options.parent;
    this.text = options.text;
    this.instruction = options.instruction;
    this.sectionId = options.sectionId;
    this.responseType = options.responseType;
    this.action = options.action;
    this.actionCriteria = options.actionCriteria;
    this.category = options.category;
    this.type = options.type;
    this.ranking = options.ranking;
    this.workstream = options.workstream;
    this.reviewType = options.reviewType;
    this.showSmall = options.showSmall;
    this.showLarge = options.showLarge;
    this.hasChildren = options.hasChildren;
    this.children = options.children;
    this.evidence = options.evidence;


    //new props
    this.actionItems = options.actionItems || [];
    this.comments = options.comments || null;
    this.response = options.response || null;
    this.$$section = section || null;

    this.getChildren = function () {
        return this.$$section.getChildrenOf(this);
    };

    this.newActionItem = function () {
        return new ActionItem(this);
    };

    this.saveActionItem = function (actionItem) {
        if (!(actionItem instanceof ActionItem)) {
            $log.error("Indicator::saveActionItem() - parameter actionItem must be instance of ActionItem class. " +
                "Please use Indicator::newActionItem() to instantiate new ActionItems", actionItem);
        }

        if (actionItem.id !== null && this.getActionItem(actionItem.id) !== undefined) {
            //objects are updated by 2 way data binding. so just return
            return;
        }

        actionItem.id = uuid.generate();

        this.actionItems.push(actionItem);


    };

    this.getParentIndicator = function() {

        if(this.parent != 0 && (!this.$$parent)) {
            this.$$parent = this.$$section.getIndicator(this.parent);
        }

        return this.$$parent;

    };

    /**
     * @name Indicator#removeActionItem
     * @param actionItem {ActionItem}
     * @returns {Indicator}
     */
    this.removeActionItem = function (actionItem) {
        var idx = this.actionItems.indexOf(actionItem);
        this.actionItems.splice(idx, 1);
        return this;
    };

    /**
     * @name Indicator#clearActionItems Clears all action items for this indicator
     * Displays a confirmation which cancels
     *
     */
    this.clearActionItems = () => {
        $window.Messenger.options = {
            theme: 'future'
        };


        if (this.actionItems.length > 0) {

            var msg = $window.Messenger().post({
                message: 'There are existing Action Items for this indicator, if you change your response the existing Action Items will be deleted. Is this Ok?',
                type: 'error',
                actions: {
                    OK: {
                        label: "OK",
                        action: () => {
                            this.actionItems = [];
                            deferred.resolve();
                            return msg.update({
                                message: "Action Items Cleared.",
                                type: 'success',
                                actions: false
                            })
                        }
                    },
                    cancel: {
                        label: "Cancel",
                        action: () => {
                            deferred.reject()
                            return msg.close();
                        }
                    }
                }
            });

            var deferred = $q.defer();

            return deferred.promise;
        }

    };

    this.getActionItem = function (id) {
        return this.actionItems.find((ai) => ai.id == id);
    };

    this.actionItems = this.actionItems.map((actionItem) => {
        if (!(actionItem instanceof ActionItem)) {
            actionItem = new ActionItem(this, actionItem);
        }

        return actionItem;
    });

    if (this.responseType === 'DATE' && this.response !== null) {
        this.response = new Date(this.response);
    }



}

/**
 * @name IndicatorData
 * @param indicator {Indicator}
 * @constructor
 */
function IndicatorData(indicator) {

    this.id = indicator.id;
    this.code = indicator.code;
    this.actionItems = indicator.actionItems;
    this.response = indicator.response;
    this.comments = indicator.comments;
}

/**
 * @name SectionData
 * @param section {Section}
 * @constructor
 */
function SectionData(section) {
    this.id = section.id;
    this.category = section.category;
    this.indicators = section.indicators.map((item) => {

        return new IndicatorData(item);

    });
}

/**
 * @name PersonData
 * @param person {Person}
 * @constructor
 */
function PersonData(person) {

    this.id = person.id;

    this.sections = person.sections.map((item) => {
        return new SectionData(item);
    });
}

/**
 * @name ReviewData
 * @param review {Review}
 * @constructor
 */
function ReviewData(review) {

    this.id = review.id;

    //strip people object to prevent duplicate Person Indicator Data
    this.unit = {
        "id": review.unit.id,
        "orgUnit": review.unit.orgUnit,
        "costCentre": review.unit.costCentre,
        "district": review.unit.district,
        "type": review.unit.type,
        "serviceType": review.unit.serviceType,
        "cluster": review.unit.cluster,
        "unitClass": review.unit.unitClass
    };

    this.filename = "./data/reviews/review-" + this.unit.id + ".json";
    this.createdBy = review.createdBy;
    this.lastEdit = review.lastEdit;
    this.lastEditBy = review.lastEditBy;

    this.sections = review.sections.map((item) => {
        return new SectionData(item);
    });

    this.people = review.people.map((item) => {
        return new PersonData(item);
    });
}