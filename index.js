"use strict";


var Converter = require('csvtojson').Converter;
var converter = new Converter({});
var fs = require('fs');
var $log = console;

var ReviewClasses = require('./classes/review.classes');
var clients = require("./data/clients.json");
var indicators = require("./data/indicators.json");
var units = require('./data/units.json');
var Person = ReviewClasses.Person;

/**
 * People parsing functions
 * @type {People}
 */
var People = require("./lib/person");

converter.on("end_parsed", function(json) {
    
    var people = new People(json, clients, indicators, units);

    console.log(people.parsed);

    // fs.writeFileSync("./data/V37.json", JSON.stringify(json, null, 2), 'utf-8');
    
});


fs.createReadStream("./data/V37.csv").pipe(converter);




