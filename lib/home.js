module.exports = ParseHome;

var ReviewClasses = require('../classes/review.classes');

var indicators = require("../data/indicators.json");

var Review = ReviewClasses.Review;



var $log = console;

function ReviewImport(data, indicators, units, costCentre) {
    var _data = data,
        _indicators

}

function ParseHome(json) {
    var homeIndicators = indicators.filter((indicator) => indicator.category == "HOME");
    var homeActionItems = getHomeActionItems(json);
    var homeSections = getHomeSections(homeIndicators, homeActionItems);
    return homeSections;
}

function getHomeActionItems(json) {
    var homeActionItems = [];

    json.forEach((item) => {
        if(item.category === "HOME") {
            var actionItem = {
                            code: item.code,
                            response: item.response,
                            comments: item.comments,
                            details: item.actionItemDescription,
                            dueDate: item.actionItemDueDate
                            };
            homeActionItems.push(actionItem);
        }
    });

    return homeActionItems;
}

function getHomeSections(indicators, actionItems) {
    var sectionIndexes = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
    var sections = [];
    sectionIndexes.forEach((index) => {
        var section = {};
        var name = '';
        var category = '';
        var id = 0;
        var filledIndicators = [];
        var filteredIndicators = indicators.filter((indicator) => indicator.sectionId == index);
        var filteredActionItems = actionItems.filter((actionItem) => actionItem.code.startsWith(index));
        filteredIndicators.forEach((indicator) => {
            var actionItems = [];
            var response = '';
            var comments = '';
            name = indicator.sectionText;
            category = indicator.category;
            id = indicator.sectionId;
            filteredActionItems.forEach((actionItem) => {
                if (actionItem.code === indicator.code) {
                    response = actionItem.response;
                    comments = actionItem.comments;
                    var simpleActionItem = {
                        details: actionItem.details,
                        dueDate: actionItem.dueDate
                    };
                    actionItems.push(simpleActionItem);
                }
            });
            indicator.response = response;
            indicator.comments = comments;
            indicator.actionItems = actionItems;
            delete indicator['sectionId'];
            delete indicator['sectionText'];
            delete indicator['category'];
            filledIndicators.push(indicator);
        });
        section.id = id;
        section.name = name;
        section.category = category;
        section.indicators = filledIndicators;
        sections.push(section);
    });
    return sections;
}