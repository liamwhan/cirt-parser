//region Delete when importing into Angular
module.exports = People;

var $log = console;
//endregion

/**
 * Parsing functions for people data
 *
 * It has the following external dependencies:
 * Person (ReviewService)
 *
 * @namespace People
 * @param data {Array} The CSV data parsed into an Array of JS Objects
 * @param allClients {Array} All client data. In app this is the return value of ResidentsService
 * @param indicators {Array} All indicator data. In app this is the return value of indicatorsService
 * @param costCentre {String} Unit Cost Centre identifier
 * @constructor
 */
function People(data, allClients, indicators) {

    //region Private members
    var _data = data,
        _allClients = allClients,
        _indicators = indicators.filter((indicator) => indicator.category === "PERSON")
        ;
    
    
    /**
     *
     * @param unitPeople
     * @param unique
     * @returns {Array.<*>}
     * @private
     */
    function matchPeople(unitPeople, unique) {
        
        if (unitPeople.length !== unique.length) {
            $log.warn("The number of clients reviewed does not match the number of clients listed under the Unit.\nUnit Clients: ", unitPeople.length, "\nReview Clients:", unique.length);
            
            if (unique.length > unitPeople.length) {
                $log.warn("This review has data for more people than are listed under the unit. Data will be missing.");
            }
        }
        
        var people = [];
        var unmatched = [];

        unique.forEach((personName) => {
            var distances = [];
            var personObject;
            
            unitPeople.forEach((person, index, array) => {
                var uName = person.firstName + " " + person.lastName;
                var similarity = levenshteinDistance(personName, uName);
                
                if (similarity === 0) {
                    
                    personObject = person;
                    people.push({personName: personName, match: personObject});
                    
                } else {
                    
                    distances.push({person: person, score: similarity});
                    
                }
                
            });
            
            if (!personObject) {
                unmatched.push({personName: personName, distance: distances});
            }
            
            
        });
        
        $log.log(people.length + " exact matches.");
        
        var approxMatches = unmatched.map((item) => {
            
            var minDistance = getMinDistance(item.distance);
            return {personName: item.personName, match: minDistance}
            
        });
        
        var result = people.concat(approxMatches);
        
        $log.log(approxMatches.length + " non-exact matches.");
        $log.log("Operation Complete.\n" + result.length + " out of " + unique.length + " people matched.");
        
        
        return result;
        
    }
    
    /**
     * returns the object with the minimum distance score
     * @param {Array} distance
     * @private
     */
    function getMinDistance(distance) {
        var min;
        var minItem;

        distance.forEach((item) => {
            if (!min || min > item.score) {
                min = item.score;
                minItem = item.person;
            }
        });

        return minItem;
    }

    /**
     * Levenshtein Distance algorithm. Helper function that calculates a score that represents the similarity between 2 strings
     * @param str1
     * @param str2
     * @private
     * @returns {Number}
     */
    function levenshteinDistance(str1, str2) {
        var m = str1.length,
            n = str2.length,
            d = [],
            i, j;

        if (!m) return n;
        if (!n) return m;

        for (i = 0; i <= m; i++) d[i] = [i];
        for (j = 0; j <= n; j++) d[0][j] = j;

        for (j = 1; j <= n; j++) {
            for (i = 1; i <= m; i++) {
                if (str1[i - 1] == str2[j - 1]) d[i][j] = d[i - 1][j - 1];
                else d[i][j] = Math.min(d[i - 1][j], d[i][j - 1], d[i - 1][j - 1]) + 1;
            }
        }
        return d[m][n];
    }

    //endregion
    
    /**
     * Matches people names from XL file to people from Clients.json and returns a map of matches
     * uses
     * @param reviewData {Object}
     * @function
     */
    this.parse = () => {

        var unique = this.getUnique(_data);
        var unitPeople = this.getByUnit(_allClients, "V37");
        var people = matchPeople(unitPeople, unique);

        return people;

    };
    
    
    this.getUnique = (reviewData) => {
        var unique = [];
        reviewData.map((item) => {
            if (item.personName !== "") {
                if (unique.indexOf(item.personName) < 0) {
                    unique.push(item.personName);
                    return item.personName;
                }

            }
        });

        return unique;
    };

    this.getByUnit = (allPeople, costCentre) => {

        return allPeople.filter((person) => person.costCentre === costCentre);

    };


    this.mapped = this.parse();

    
    this.extractIndicatorData = function (json) {
        var indicatorData = [];

        json.forEach((item) => {
            if (item.category === "PERSON") {
                var indicator = {
                    code: item.code,
                    response: item.response,
                    comments: item.comments,
                    details: item.actionItemDescription,
                    dueDate: item.actionItemDueDate,
                };
                indicatorData.push(indicator);
            }
        });

        return indicatorData;
    };
    
    this.getSections = function (indicators, indicatorData) {
        var sectionIndexes = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'];
        var sections = [];
        sectionIndexes.forEach((index) => {
            var section = {};
            var category = '';
            var id = 0;
            var filledIndicators = [];
            var filteredIndicators = indicators.filter((indicator) => indicator.sectionId == index);
            var filteredindicatorData = indicatorData.filter((data) => data.code.startsWith(index));
            
            filteredIndicators.forEach((indicator) => {

                var indicatorData = [];
                var response = '';
                var comments = '';
                category = indicator.category;
                id = indicator.sectionId;
                
                filteredindicatorData.forEach((_item) => {
                    if (_item.code === indicator.code) {
                        try {
                            response = (typeof _item.response == 'string') ? _item.response.toUpperCase() : _item.response;
                            comments = _item.comments;
                            var simpleActionItem = {
                                details: _item.details,
                                dueDate: _item.dueDate
                            };
                            indicatorData.push(simpleActionItem);
                        } catch (e) {
                            debugger;
                            
                        }
                       
                    }
                    
                });
                
                indicator.response = response;
                indicator.comments = comments;
                indicator.actionItems = indicatorData;
                
                delete indicator['sectionId'];
                delete indicator['sectionText'];
                delete indicator['category'];
                
                filledIndicators.push(indicator);
            });
            section.id = id;
            section.category = category;
            section.indicators = filledIndicators;
            sections.push(section);
        });
        return sections;
    };
    
    
    this.parseIndicators = () => {
        
        var parsed = this.mapped.map((item) => {
            var personData = _data.filter((x) => x.personName === item.personName);
            var indicatorData = this.extractIndicatorData(personData);
          
            item.match.sections = this.getSections(_indicators, indicatorData);

            return item.match;
        });
        return parsed;
    };
    
   require('fs').writeFileSync('./test.json', JSON.stringify(this.parseIndicators(), null, 2));
    
}











